//
//  EmojiDetailsViewController.swift
//  PG12Alfredo_iOS_Emoji_Collection_View
//
//  Created by Alfredo Quintero Tlacuilo on 2018-07-09.
//  Copyright © 2018 Alfredo Quintero Tlacuilo. All rights reserved.
//

import UIKit

class EmojiDetailsViewController: UIViewController {
    
    @IBOutlet weak var emojiLabel: UILabel!
    
    public var emoji: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Unwrap
        if let emoji = emoji {
            emojiLabel.text = emoji
        }
    }

}

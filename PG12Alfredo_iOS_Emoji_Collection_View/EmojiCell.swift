//
//  EmojiCell.swift
//  PG12Alfredo_iOS_Emoji_Collection_View
//
//  Created by Alfredo Quintero Tlacuilo on 2018-07-09.
//  Copyright © 2018 Alfredo Quintero Tlacuilo. All rights reserved.
//

import UIKit

class EmojiCell: UICollectionViewCell {

    @IBOutlet weak var emojiLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func setContents(emoji: String) {
        emojiLabel.text = emoji
    }

}

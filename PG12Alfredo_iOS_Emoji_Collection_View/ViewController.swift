//
//  ViewController.swift
//  PG12Alfredo_iOS_Emoji_Collection_View
//
//  Created by Alfredo Quintero Tlacuilo on 2018-07-09.
//  Copyright © 2018 Alfredo Quintero Tlacuilo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let emojis: [[String]] = [ ["🤠", "😎", "🇲🇽", "🐇"], ["🌝", "🌚", "🐢", "👨🏽‍💻"], ["🤤", "🤓", "💩", "🥐"] ]

    @IBOutlet weak var emojiCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emojiCollectionView.dataSource = self
        emojiCollectionView.delegate = self
        
        // Register the nib
        
        emojiCollectionView.register(UINib(nibName: "EmojiCell", bundle: nil), forCellWithReuseIdentifier: "EmojiCell")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EmojiDetailsSegue" {
            if let dest = segue.destination as? EmojiDetailsViewController {
                if let indexpaths = emojiCollectionView.indexPathsForSelectedItems {
                    // Get the first one, I guess
                    let indexpath = indexpaths[0]
                    // Send the emoji
                    dest.emoji = emojis[indexpath.section][indexpath.row]
                }
            }
        }
    }
    
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // Get THE CELL
        let cell = emojiCollectionView.dequeueReusableCell(withReuseIdentifier: "EmojiCell", for: indexPath) as! EmojiCell
        
        print(cell)
        
        // The contents
        cell.setContents(emoji: emojis[indexPath.section][indexPath.row])
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return emojis.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // This is kind of hardcoded
        return emojis[0].count
    }
}

extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Go to details
        performSegue(withIdentifier: "EmojiDetailsSegue", sender: self)
    }
}
